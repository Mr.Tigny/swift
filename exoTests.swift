import XCTest
import class Foundation.Bundle

//Aller à la racine pour y cree les tables
router.get("/") { request, response, next in

    do {
        try Sujet.createTableSync()
        try "INSERT * INTO \"Sujet\" Values ('TestSujet, 1)"
        try Tag.createTableSync()
        try "INSERT * INTO \"Tag\" Values ('TestTag, 2)"
        try Url.createTableSync()
        try "INSERT * INTO \"Table\" Values ('TestTable, 3)"
    } catch let error {
        print "Table non créé"
    }

//cree un chemin et y afficher la table Sujet
router.get("/Sujet") { request, response, next in
    do {
        try print Sujet()
    } catch let error {
        print "Sujet non récuperé"
    }
    next()
    //Selectionner tout l'interieur de la table Sujet
    router.get("/Sujet/enum") { request, response, next in
    do {
        try "SELECT * FROM \"Sujet\""()
    } catch let error {
        print "Interieur de la table Sujet non récuperé"
    }
    next()

//cree un chemin et y afficher la table tag
router.get("/Tag") { request, response, next in
    do {
        try print Tag()
    } catch let error {
        print "Tag non récuperé"
    }
    next()

//Selectionner tout l'interieur de la table Tag
    router.get("/Tag/enum") { request, response, next in
    do {
        try "SELECT * FROM \"Tag\""()
    } catch let error {
        print "interieur de la table Tag non récuperé"
    }
    next()

//cree un chemin et y afficher la table URL
router.get("/Url") { request, response, next in
    do {
        try print Url()
    } catch let error {
        print "Url non récuperé"
    }
    next()

//Selectionner tout l'interieur de la table URL 
router.get("/Url/enum") { request, response, next in
    do {
        try "SELECT * FROM \"Url\""()
    } catch let error {
        print "Interieur de la table URL non récuperé"
    }
    next()

Kitura.addHTTPServer(onPort: 8080, with: router)
Kitura.run()